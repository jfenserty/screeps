/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.collector');
 * mod.thing == 'a thing'; // true
 */

 //The collector will search for dropped energy on the ground, pick it up, and store it in buildings that hold energy. 
 //If they have a work part, they will try to build, then upgrade.
 //My smaller ones are generally spawned with no work parts.
 //Prioritizes depositing in spawns and extensions, then everything else.
var roleCollector = {
 
    /** @param {Creep} creep **/
    run: function(creep) {
        //var theRoom = Game.rooms['W8N3']; (this concept might be useful later, but with a way to select a room)
        var droppedRes = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
            filter: (resource) => {
                return (resource.amount > 19);
            }
        });
        //var droppedRes = creep.pos.find(FIND_DROPPED_RESOURCES);
        var targetsPrime = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_SPAWN ||
                    structure.structureType == STRUCTURE_EXTENSION) &&
                    structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
            }
            
        });
        var targets2ndary = creep.pos.findClosestByPath(FIND_STRUCTURES, {
        filter: (structure) => {
            return (structure.structureType == STRUCTURE_TOWER ||
                structure.structureType == STRUCTURE_CONTAINER ||
                structure.structureType == STRUCTURE_STORAGE) &&
                structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
        }
        
    });
        var unBuilts = creep.room.find(FIND_CONSTRUCTION_SITES);

    if (creep.memory.storing && creep.store[RESOURCE_ENERGY] == 0) {
        creep.memory.storing = false;
        creep.say('🐝 pick up');
    }
    if (!creep.memory.storing && (creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0 || !droppedRes)) {
        creep.memory.storing = true;
        creep.say('🚌 deposit')
    }

    if(droppedRes && !creep.memory.storing) {
        if(creep.pickup(droppedRes) == ERR_NOT_IN_RANGE) {
            creep.moveTo(droppedRes, {visualizePathStyle: {stroke: '#998a3d'}});
        }
    }

    if(targetsPrime !== null && creep.memory.storing) {
        if(creep.transfer(targetsPrime, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(targetsPrime, {visualizePathStyle: {stroke: '#ffffff'}});
        }
    }
    else if(targets2ndary !== null && creep.memory.storing) {
        if(creep.transfer(targets2ndary, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(targets2ndary, {visualizePathStyle: {stroke: '#ffffff'}});
        }
    }

    //Collectors with a work part (currently only spawned above a minimum energy availability) will try to build and then try to upgrade.
    else if((creep.getActiveBodyparts(WORK) > 0) && creep.memory.storing) {
        if (unBuilts.length > 0) {
            if (creep.build(unBuilts[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(unBuilts[0], { visualizePathStyle: { stroke: '#ffffff' } });
            }
        }
        else if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
            creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#f403fc' } });
        }
    }
}
}
module.exports = roleCollector;