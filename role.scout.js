/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.scout');
 * mod.thing == 'a thing'; // true
 */

// pick a point with a random x value that has, for example, a y value of 0, and try to go to it. After enough ticks, try another x value.
// and do the same with x and y reversed in the code?

//Below are some useful snippets from my first attempt to write a scout that targeted rooms.
//         const currentRoom = creep.room;
//         const roomExits = Game.map.describeExits(currentRoom);
//         let randomChoice = Math.floor(Math.random() * 5);
//         const target = roomExits[randomUsed];

//Below are my comments from my first expander role attempt, which had no code in it.
/* This creep will consist of just a move and a claim part. It should only spawn 
when there's an adjacent room that doesn't belong to anybody yet. 
Start with: if in a room that's already claimed, set some variable to being false. Make it true once they're in an unclaimed room.
Use find(FIND_EXIT) with a random selection thingy (use a Math method) to pick from the array of exits
Move to the chosen exit.
If the new room has an unclaimed spawn, claim it. 
Repeat (if in a claimed room, should proceed to pick another exit)
*/

//Below here is a copy paste of Jason's initial scout code. I can probably use it with Jason's idea that I wrote in the comments above with the random values.
// var roleScout = {

//     /** @param {Creep} creep **/
//     run: function(creep) {
//         if(creep.room.name == "W7N7"){
//             //creep.moveTo(0,13);
//             console.log('scount in W7N7')
//             console.log(creep.memory.dirPref);
//             if(creep.memory.dirPref < 0.5){
//                 creep.moveTo(25,0);
//             }else if(creep.memory.dirPref < 1.5){
//                 creep.moveTo(0,25);
//             }else if(creep.memory.dirPref < 2.5){
//                 creep.moveTo(49,25);
//             }else{
//                 creep.moveTo(25,49);
//             }
//         }else{
//             if(!creep.room.controller.my) {
//                 if(creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE) {
//                     creep.moveTo(creep.room.controller);
//                 }
//                 else{
//                     creep.claimController(creep.room.controller);
//                 }
//             }else{
//                 creep.moveTo(25,25);
//             }

//         }
//     }
// };

// module.exports = roleScout;

/* 

THIS IS WHAT IT DOES RN:
Ok, actually, how about it default tries to go to 0,0. if (Game.time % n == 0), increment x-coordinate by a random distance (from 0-19, based on Math.random). 
Once we get to (49, 0), begin incrementing y instead of x. 
At (49,49), start decrementing x.
And at (0,49), start decrementing y.
All my scouts SHOULD take at least slightly different paths now.
They all spawn and try to go to a target, then start changing that target in a clockwise direction around the room every so many ticks.
If controller is owned by someone else, attack it (with claim parts). If it's neutral, claim it.
  Will obviously only function if I put claim parts on the scouts.
IDEA TO TRY LATER: make scouts switch their move target changing direction from clockwise to counterclockwise every so many ticks (a larger interval than just the target change)
*/

var roleScout = {
  /** @param {Creep} creep **/
  run: function (creep) {

    const timeLastDigit = Number(Game.time.toString().charAt(Game.time.toString().length - 1));
    const randomDistance = Math.floor(Math.random() * 20);

    // if (creep.room.controller.my) 
    //   creep.memory.scouting = true;
    // if (!creep.room.controller.my && creep.getActiveBodyparts(CLAIM) > 0)
    //   creep.memory.scouting = false;
    // creep.memory.scouting = true;
    if (creep.room.controller.my)
     creep.memory.scouting = true;
    if(!creep.memory.direction) {
      if(timeLastDigit > 4)
        creep.memory.yTarget = 49;
      else creep.memory.yTarget = 0;
      if (Math.random() > 0.4)
        creep.memory.xTarget = 49;
        else creep.memory.xTarget = 0;
    } //Spawning creep targets either y=0 or y=49, and picks x-value of 49.
    if (Game.time % 12 == 0) { //The tick interval before it changes it's target position.
      // console.log(Game.time);
      switch (creep.memory.direction) {
        case "North":
          creep.memory.xTarget += randomDistance; //so note that if you use timeLastDigit instead of randomDista, you create a flaw because both the tick interval and the distance changed are tied together, so the coordinate adjustment will always be 0 if the interval is 10.
          break;
        case "East":
          creep.memory.yTarget += randomDistance;
          break;
        case "South":
          creep.memory.xTarget -= randomDistance;
          break;
        case "West":
          creep.memory.yTarget -= randomDistance;
          break;
      }
    }
    if (creep.memory.xTarget <= 0 && creep.memory.yTarget <= 0) {
      creep.memory.direction = "North";
      creep.memory.xTarget = 0; //To prevent xTarget and yTarget from overflowing past 49 or 0, need to reset them on direction change.
      creep.memory.yTarget = 0;}
    if (creep.memory.xTarget >= 49 && creep.memory.yTarget <= 0) {
      creep.memory.direction = "East";
      creep.memory.xTarget = 49;
      creep.memory.yTarget = 0;}
    if (creep.memory.xTarget >= 49 && creep.memory.yTarget >= 49) {
      creep.memory.direction = "South";
      creep.memory.xTarget = 49;
      creep.memory.yTarget = 49;}
    if (creep.memory.xTarget <= 0 && creep.memory.yTarget >= 49) {
      creep.memory.direction = "West";
      creep.memory.xTarget = 0;
      creep.memory.yTarget = 49;}
    // console.log("x:" + creep.memory.xTarget);
    // console.log("y:" + creep.memory.yTarget);
    // console.log(creep.memory.direction);
    if (creep.memory.scouting) {
      creep.moveTo(creep.memory.xTarget, creep.memory.yTarget, { visualizePathStyle: { stroke: '#fffff' } });
      //creep.say(creep.memory.direction);
      creep.say("🧚🏼fuck u", true)
    }
  

    if (!creep.room.controller.my)
      (creep.memory.scouting = false)
    if (!creep.memory.scouting) {
      creep.say("🦾gimme");
      if(creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE)
        creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#55555' } });
      else if(creep.claimController(creep.room.controller) ==  ERR_INVALID_TARGET)
        creep.attackController(creep.room.controller)
    }

  }
};
module.exports = roleScout;
