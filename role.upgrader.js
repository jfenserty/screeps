/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.upgrader');
 * mod.thing == 'a thing'; // true
 */
//Takes energy from containers (or sources if necessary), uses it to build, or upgrade if there's nothing to build.

var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var sources = creep.pos.findClosestByPath(FIND_SOURCES);
        var storage = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_CONTAINER ||
                        structure.structureType == STRUCTURE_STORAGE) &&
                    (structure.store.getUsedCapacity(RESOURCE_ENERGY) > 0);
            }
           })

        if(creep.memory.upgrading && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.upgrading = false;
            creep.say('🔄 harvest');
        }
        if(!creep.memory.upgrading && creep.store.getFreeCapacity() == 0) {
            creep.memory.upgrading = true;
            creep.say('⚡ upgrade');
        }

        if(creep.memory.upgrading) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#f403fc'}});
            }
        }
        else if(storage !== null) {
            if (creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
            creep.moveTo(storage, RESOURCE_ENERGY, { visualizePathStyle: { stroke: '#ffaa00' } });}
        else {
            if (creep.harvest(sources) == ERR_NOT_IN_RANGE)
                creep.moveTo(sources, { visualizePathStyle: { stroke: '#ffaa00' } });
        }
    }
};

module.exports = roleUpgrader;