/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.builder');
 * mod.thing == 'a thing'; // true
 */
//Takes energy from containers (or sources if necessary), uses it to build, or upgrade if there's nothing to build.

var roleBuilder = {

    /** @param {Creep} creep **/
    run: function (creep) {
        var source = creep.pos.findClosestByPath(FIND_SOURCES);
        var storage = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (structure) => {
                return ((structure.structureType == STRUCTURE_CONTAINER || 
                         structure.structureType == STRUCTURE_STORAGE) &&
                    (structure.store.getUsedCapacity(RESOURCE_ENERGY) > 0));
            }
           })
        var target = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES);
        var droppedRes = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
            filter: (resource) => {
                return (resource.amount > 19);
            }
        });

        if (creep.memory.building && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.building = false;
            creep.say('🔄 harvest');
        }
        if (!creep.memory.building && creep.store.getFreeCapacity() == 0) {
            creep.memory.building = true;
            creep.say('🚧 build');
        }
        if (creep.memory.building && target == null) {
            creep.say('⚡ upgrade');
        }


        if (creep.memory.building) {
            if (target == null) {
                // if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                //     creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#f403fc' } });
                // }
                if(!creep.pos.inRangeTo(creep.room.controller, 3)) {
                    creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#f403fc' } });
                    return;
                }
                creep.upgradeController(creep.room.controller)
                return;
            }
            if(!creep.pos.inRangeTo(target, 3)) {
                creep.moveTo(target, { visualizePathStyle: { stroke: '#ffffff' } });
                return;
            }
            creep.build(target);
            return;
        }
        //Want: prioritize picking up from containers or dropped resources, whichever is closer. Then, look to sources last.
        else if(storage !== null) {
            if (creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
            creep.moveTo(storage, RESOURCE_ENERGY, { visualizePathStyle: { stroke: '#ffaa00' } });}
        else if(droppedRes) {
            if(creep.pickup(droppedRes) == ERR_NOT_IN_RANGE) {
                creep.moveTo(droppedRes, {visualizePathStyle: {stroke: '#998a3d'}});
            }
        }
        else {
            if (creep.harvest(source) == ERR_NOT_IN_RANGE)
                creep.moveTo(source, { visualizePathStyle: { stroke: '#ffaa00' } });
        }
        }
    }
    // if(creep.pos.inRangeTo(source.pos, 1)) {
    //     creep.harvest(source);
    // }
    // else creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});



module.exports = roleBuilder;