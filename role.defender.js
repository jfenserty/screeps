/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.defender');
 * mod.thing == 'a thing'; // true
 */
// Prioritizes attacking enemy creeps. Otherwise harvests energy from containers, then sources, and uses it to repair structures.
// Doesn't seem to work yet (at least the repairing part)

var roleDefender = {

    /** @param {Creep} creep **/
    run: function (creep) {

        var hostileTargets = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        // var storage = creep.pos.findClosestByPath(FIND_STRUCTURES, {
        //     filter: (structure) => {
        //         return (structure.structureType == STRUCTURE_CONTAINER) &&
        //             (structure.store.getUsedCapacity(RESOURCE_ENERGY) > 0);
        //     }
        // });
        // var sources = creep.pos.findClosestByPath(FIND_SOURCES);
        // var repairTargets = creep.pos.findClosestByRange(FIND_STRUCTURES, {
        //     filter: (structure) => structure.hits < structure.hitsMax
        // });

        if (hostileTargets) {
            creep.memory.attacking = true;
            // creep.memory.repairing = false;
            creep.say('🔫 defend');
        }
        else creep.memory.attacking = false;

        // if (!creep.memory.attacking && creep.store.getFreeCapacity([RESOURCE_ENERGY]) > 0) {
        //     creep.memory.repairing = false;
        //     creep.say('🔄 harvest');
        // }
        // if (!creep.memory.attacking && creep.store.getFreeCapacity() == 0 && repairTargets) {
        //     creep.memory.repairing = true;
        //     creep.say('🔧 repair');
        // }


        if (creep.memory.attacking) {
            if (creep.attack(hostileTargets) == ERR_NOT_IN_RANGE) {
                creep.moveTo(hostileTargets, { visualizePathStyle: { stroke: '#8B0000' } });
            }
        }

        // if (!creep.memory.attacking && !creep.memory.repairing) {
        //     if (storage !== null) {
        //         if (creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
        //             creep.moveTo(storage, RESOURCE_ENERGY, { visualizePathStyle: { stroke: '#ffaa00' } });
        //     }
        //     else if (creep.harvest(sources) == ERR_NOT_IN_RANGE) {
        //         creep.moveTo(sources, { visualizePathStyle: { stroke: '#ffaa00' } });
        //     }
        // }

        // if (!creep.memory.attacking && creep.memory.repairing) {
        //     if (creep.repair.repairTargets == ERR_NOT_IN_RANGE) {
        //         creep.moveTo(repairTargets, { visualizePathStyle: { stroke: '#03fc7f' } });
        //     }
        // }

    }
};

module.exports = roleDefender;