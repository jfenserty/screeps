/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.repairer');
 * mod.thing == 'a thing'; // true
 */
//Takes energy from containers (or sources), repairs structures.
//Should add to it: exclude walls as targets.
//Should also make it build/upgrade as alternative tasks.

var roleRepairer2 = {
  /** @param {Creep} creep **/
  run: function (creep) {
    var source = creep.pos.findClosestByPath(FIND_SOURCES);
    var storage = creep.pos.findClosestByPath(FIND_STRUCTURES, {
      filter: (structure) => {
        return (
          (structure.structureType == STRUCTURE_CONTAINER ||
            structure.structureType == STRUCTURE_STORAGE ||
            structure.structureType == STRUCTURE_EXTENSION) &&
          structure.store.getUsedCapacity(RESOURCE_ENERGY) > 0
        );
      },
    });
    var unBuilts = creep.room.find(FIND_CONSTRUCTION_SITES);

    if (creep.memory.repairing && creep.store[RESOURCE_ENERGY] == 0) {
      creep.memory.repairing = false;
      creep.say("🔄 harvest");
    }
    if (!creep.memory.repairing && creep.store.getFreeCapacity() == 0) {
      creep.memory.repairing = true;
      creep.say("🚧 repair");
    }

    if (creep.memory.repairing) {
      var targets = creep.room.find(FIND_STRUCTURES, {
        filter: (object) => object.hits < object.hitsMax / 1.02,
      }); //Would like to change this part to set soft caps for rampart/wall repairs.
      if (targets.length) {
        return moveAndRepair(creep, targets);
      }
      //If there are no targets to repair, try to build.
      if (unBuilts.length > 0) {
        return moveAndBuild(creep, unBuilts);
      }
      //If nothing to build either, try to upgrade.
      return moveAndUpgrade(creep);
    }
    if (storage !== null) {
      return getFromStorage(creep, storage);
    }
      return harvestFromSources(creep, source);
  },
};

function moveAndRepair(creep, targets) {
  //if(creep.repair(targets[0]) == ERR_NOT_IN_RANGE) {
  if(!creep.pos.inRangeTo(targets[0], 3)) {
  creep.moveTo(targets[0], {
    visualizePathStyle: { stroke: "#03fc7f" },
  });
  return;
}
  creep.repair(targets[0]);
  return;
}

function moveAndBuild(creep, unBuilts) {
  //if(creep.build(unBuilts[0]) == ERR_NOT_IN_RANGE){
  if(!creep.pos.inRangeTo(unBuilts[0], 3)) {
  creep.moveTo(unBuilts[0], {
    visualizePathStyle: { stroke: "#ffffff" },
  });
  return;
}
  creep.build(unBuilts[0]);
  return;
}

function moveAndUpgrade(creep) {
  if (!creep.pos.inRangeTo(creep.room.controller, 3)) {
  creep.moveTo(creep.room.controller, {
    visualizePathStyle: { stroke: "#ffffff" },
  });
  return;
}
  creep.upgradeController(creep.room.controller);
  return;
}

function getFromStorage(creep, storage) {
  if(!creep.pos.inRangeTo(storage, 1)) {
  creep.moveTo(storage, RESOURCE_ENERGY, {
    visualizePathStyle: { stroke: "#ffaa00" },
  });
  return;
}
  creep.withdraw(storage, RESOURCE_ENERGY);
  return;
}

function harvestFromSources(creep, source) {
  if (!creep.pos.inRangeTo(source, 1)) {
    creep.moveTo(source, {
    visualizePathStyle: { stroke: "#ffaa00" } });
    return;
    };
  creep.harvest(source);
  return;
}

module.exports = roleRepairer2;
