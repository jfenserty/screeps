/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.attacker');
 * mod.thing == 'a thing'; // true
 */
//For now, will just use the scout code but specifically target invader cores for destruction if they're present.

var roleAttacker = {
  /** @param {Creep} creep **/
  run: function (creep) {
    const timeLastDigit = Number(
      Game.time.toString().charAt(Game.time.toString().length - 1)
    );
    const randomDistance = Math.floor(Math.random() * 20);
    const hostileTargets = creep.room.find(FIND_HOSTILE_STRUCTURES, {
      filter: { structureType: STRUCTURE_INVADER_CORE },
    }); //&& creep.room.find(FIND) //This is the variable I can expand to more things later on perhaps.

    if (!hostileTargets.length) creep.memory.scouting = true;
    if (!creep.memory.direction) {
      if (timeLastDigit > 4) creep.memory.yTarget = 49;
      else creep.memory.yTarget = 0;
      if (Math.random() > 0.4) creep.memory.xTarget = 49;
      else creep.memory.xTarget = 0;
    } //Spawning creep targets either y=0 or y=49, and picks x-value of 49.
    if (Game.time % 12 == 0) {
      //The tick interval before it changes it's target position.
      // console.log(Game.time);
      switch (creep.memory.direction) {
        case "North":
          creep.memory.xTarget += randomDistance; //so note that if you use timeLastDigit instead of randomDista, you create a flaw because both the tick interval and the distance changed are tied together, so the coordinate adjustment will always be 0 if the interval is 10.
          break;
        case "East":
          creep.memory.yTarget += randomDistance;
          break;
        case "South":
          creep.memory.xTarget -= randomDistance;
          break;
        case "West":
          creep.memory.yTarget -= randomDistance;
          break;
      }
    }
    if (creep.memory.xTarget <= 0 && creep.memory.yTarget <= 0) {
      creep.memory.direction = "North";
      creep.memory.xTarget = 0; //To prevent xTarget and yTarget from overflowing past 49 or 0, need to reset them on direction change.
      creep.memory.yTarget = 0;
    }
    if (creep.memory.xTarget >= 49 && creep.memory.yTarget <= 0) {
      creep.memory.direction = "East";
      creep.memory.xTarget = 49;
      creep.memory.yTarget = 0;
    }
    if (creep.memory.xTarget >= 49 && creep.memory.yTarget >= 49) {
      creep.memory.direction = "South";
      creep.memory.xTarget = 49;
      creep.memory.yTarget = 49;
    }
    if (creep.memory.xTarget <= 0 && creep.memory.yTarget >= 49) {
      creep.memory.direction = "West";
      creep.memory.xTarget = 0;
      creep.memory.yTarget = 49;
    }
    // console.log("x:" + creep.memory.xTarget);
    // console.log("y:" + creep.memory.yTarget);
    // console.log(creep.memory.direction);
    if (creep.memory.scouting) {
      creep.moveTo(creep.memory.xTarget, creep.memory.yTarget, {
        visualizePathStyle: { stroke: "#fffff" },
      });
      //creep.say(creep.memory.direction);
      creep.say("🐾hunting", true);
    }

    if (hostileTargets.length) creep.memory.scouting = false;
    if (!creep.memory.scouting) {
      creep.say("🔪die,scum");
      if (creep.attack(hostileTargets[0]) == ERR_NOT_IN_RANGE) {
        creep.moveTo(hostileTargets[0], {
          visualizePathStyle: { stroke: "#8B0000" },
        });
      }
    }
  },
};

module.exports = roleAttacker;
