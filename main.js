/*
//To-do: 
//Make defenders' backup role (get energy and then repair) actually work.
//Set up stuff for the other buildings I don't use yet.
//Consider setting specific number variables like harvesterLengthNeeded and having that adjust based on energy and other variables,
//then using that variable in the spawning if statements instead of simple numbers.
// Standardize how creeps say the work they're doing.
// Come up with a system for how to color creep paths while moving.
// Auto building placement.
// Ask friends for suggestions for relevant repairer name themes.
//Make scouts/expanders/maybe attackers only spawn when above a certain controller level.
//Do some more basic refactoring to put stuff into functions (like with the manageTowers function)
//  - later maybe move them to their own modules or something?
//Add constants that are totals of all creeps in all rooms for each role, update the periodic console.log at the bottom.
//Find out how to fix this cpu thing
//  try adding a constant for each creep type that is an object recording how many there are in each room by room name, and use that in the spawning code?
//  try storing creep path in memory until they get there, instead of having them recalculate every tick. (use moveByPath())
//  try having bigger and fewer creeps
//      one way to have fewer creeps is also to combine roles (give repair job to builders)
//  don't have things like if(harvest returns error message), then move to source, because that takes up more cpu by giving more commands
//      instead use InRangeTo maybe
//Happy path coding: a series of if statements:
//                                  if (something bad)
//                                      potentially do something to fix it;
//                                      return;
//                                  at the end you get to the good thing.
//   This will be a lot easier if I make it modular like David's task things.
Tasks I should make modules for: 
build
repair 
upgrade controller
harvest from source
collect from container/storage
collect from container/storage/extension
pick up from loose ground
return to extension/spawn/container/storage
Priorities lists:
Collectors should only pick up from loose ground, and deliver to various containers in a priority list, then build/upgrade (if they have work parts)
Harvesters only should move to sources and harvest
Builders should pick up from containers/storage, then pick up loose energy, then harvest directly, then pick up from extensions. If they have energy, focus on building, then repairing, then upgrading.
Upgrader should pick up in the same order as builders, but not from extensions. Use energy only to upgrade.
Repairers should pick up in the same order as builders, and repair things that fall below a specific threshold, but with separate rules for walls/ramparts.
The various exploring roles should be dealt with later.
So I need to figure out the basic structure for how creeps run. You can see how David's use a priorities list in each role-creeptype file.


*/
var roleHarvester = require('role.harvester');
var roleBuilder = require('role.builder');
var roleUpgrader = require('role.upgrader');
var roleRepairer = require('role.repairer');
var roleDefender = require('role.defender');
var roleCollector = require('role.collector');
var roleScout = require('role.scout');
var roleAttacker = require('role.attacker');
var roleExpander = require('role.expander');
var creepNames = require('creep.names');

module.exports.loop = function () {
    for (var name in Memory.creeps) {
        if (!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }
    Object.values(Game.rooms).forEach((room) => {
    //this part should hopefully make my stuff work in all rooms I own. We'll see when we get there.
    


    var currentRoom = room;
    var currentSpawns = currentRoom.find(FIND_MY_SPAWNS);
    if (currentSpawns.length == 0)
        return;
    var availableSpawn = currentSpawns[0];
    const extensions = availableSpawn.room.find(FIND_MY_STRUCTURES, {
        filter: { structureType: STRUCTURE_EXTENSION }
    }); //Creates the variable extensions, which is an array of all the extensions by id in Spawn1's room.

    // const returnExtensionsEnergy = function () {
    //     var extensionsEnergy = 0;
    //     for (n = 0; n < extensions.length; n++) {
    //         extensionsEnergy = extensionsEnergy + extensions[n].store.getUsedCapacity(RESOURCE_ENERGY)
    //     }
    //     return extensionsEnergy;
    // } //A function that returns the total energy contained in all the extensions in Spawn1's room.
    //(currentRoom.energyAvailable
    //the above was used before I found out about energyAvailable, a property of rooms.
    //If I want to reverse the spawn expansion thing, find and replace availableSpawn to: Game.spawns['Spawn1']
    //  - also comment out the variableS above (currentSpawns and availableSpawn)
    //var currentRoom = Game.rooms['W2N8'];

    // var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
    // var collectors = _.filter(Game.creeps, (creep) => creep.memory.role == 'collector');
    // var builders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder');
    // var repairers = _.filter(Game.creeps, (creep) => creep.memory.role == 'repairer');
    // var upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');
    // var defenders = _.filter(Game.creeps, (creep) => creep.memory.role == 'defender');
    // var scouts = _.filter(Game.creeps, (creep) => creep.memory.role == 'scout');
    // var attackers = _.filter(Game.creeps, (creep) => creep.memory.role == 'attacker')
    // var expanders = _.filter(Game.creeps, (creep) => creep.memory.role == 'expander')
   

    //The spawnRoom memory addendum SHOULD make it so that every room tracks its own creep numbers.

    manageTowers(currentRoom);
    manageSpawns(currentRoom);

    function manageTowers (currentRoom) {
        //Jason's Tower Code:
        var towers = currentRoom.find(FIND_MY_STRUCTURES, { filter: { structureType: STRUCTURE_TOWER } });
        var hostiles = currentRoom.find(FIND_HOSTILE_CREEPS);
        towers.forEach(tower => tower.attack(hostiles[0]));

            // var tower = Game.rooms['W2N8'].find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
        // if (tower) {
        //     var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
        //         filter: (structure) => structure.hits < structure.hitsMax
        //     });
        //     if (closestDamagedStructure) {
        //         tower.repair(closestDamagedStructure);
        //     }

        //     var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        //     if (closestHostile) {
        //         tower.attack(closestHostile);
        //     }
        // }
    }

    function manageSpawns(currentRoom) {
        //The rooms I want to pick from for all this shit should have at least one spawn that belongs to me in them.
        let harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester' && creep.memory.spawnRoom == currentRoom.name);
        let collectors = _.filter(Game.creeps, (creep) => creep.memory.role == 'collector' && creep.memory.spawnRoom == currentRoom.name);
        let builders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder' && creep.memory.spawnRoom == currentRoom.name);
        let repairers = _.filter(Game.creeps, (creep) => creep.memory.role == 'repairer' && creep.memory.spawnRoom == currentRoom.name);
        let upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader' && creep.memory.spawnRoom == currentRoom.name);
        let defenders = _.filter(Game.creeps, (creep) => creep.memory.role == 'defender' && creep.memory.spawnRoom == currentRoom.name);
        let scouts = _.filter(Game.creeps, (creep) => creep.memory.role == 'scout' && creep.memory.spawnRoom == currentRoom.name);
        let attackers = _.filter(Game.creeps, (creep) => creep.memory.role == 'attacker' && creep.memory.spawnRoom == currentRoom.name);
        let expanders = _.filter(Game.creeps, (creep) => creep.memory.role == 'expander' && creep.memory.spawnRoom == currentRoom.name);
        

        if(collectors.length > 0){
            if (harvesters.length < 2 && currentRoom.energyAvailable >= 3000) {
                var newName = creepNames.harvesterNames[Math.floor(Math.random() * creepNames.harvesterNames.length)]+" Mega";
                //var newName = 'HarvesterMega' + Game.time;
                //console.log('Spawning new harvester: ' + newName);
                availableSpawn.spawnCreep([WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, MOVE], newName,
                    { memory: { role: 'harvester', spawnRoom: currentRoom.name } });
            }
            else if (harvesters.length < 4) {
                if(currentRoom.energyAvailable >= 1150 && currentRoom.energyAvailable < 3000) {
                    var newName = creepNames.harvesterNames[Math.floor(Math.random() * creepNames.harvesterNames.length)]+" Big";
                    //var newName = 'HarvesterBig' + Game.time;
                    //console.log('Spawning new harvester: ' + newName);
                    availableSpawn.spawnCreep([WORK, WORK, WORK, WORK, WORK, WORK, WORK, MOVE], newName,
                    { memory: { role: 'harvester', spawnRoom: currentRoom.name } });
            }
            else if (currentRoom.energyAvailable >= 800) {
                var newName = creepNames.harvesterNames[Math.floor(Math.random() * creepNames.harvesterNames.length)]+" Md";  
                //var newName = 'HarvesterMd' + Game.time;
                    //console.log('Spawning new harvester: ' + newName);
                    availableSpawn.spawnCreep([WORK, WORK, WORK, WORK, WORK, MOVE], newName,
                        { memory: { role: 'harvester', spawnRoom: currentRoom.name } });
                }
                else if (currentRoom.energyAvailable > 300) {
                    var newName = creepNames.harvesterNames[Math.floor(Math.random() * creepNames.harvesterNames.length)]+" Smol";
                    //var newName = 'HarvesterSmol' + Game.time;
                    //console.log('Spawning new harvester: ' + newName);
                    availableSpawn.spawnCreep([WORK, WORK, WORK, MOVE], newName,
                        { memory: { role: 'harvester', spawnRoom: currentRoom.name } });
                }
            }
        }
        
            // if (harvesters.length < 3) {
            //     if (currentRoom.energyAvailable < 300) {
            //         var newName = 'HarvesterMini' + Game.time;
            //         //console.log('Spawning new harvester: ' + newName);
            //         availableSpawn.spawnCreep([WORK, MOVE], newName,
            //             { memory: { role: 'harvester' } });
            //     }
            //     else 
            if(harvesters.length < 4 && currentRoom.energyAvailable <= 300) {
                if(extensions.length < 1) {
                var newName = creepNames.harvesterNames[Math.floor(Math.random() * creepNames.harvesterNames.length)]+" Mini";
                //console.log('Spawning new harvester: ' + newName);
                availableSpawn.spawnCreep([WORK, MOVE], newName,
                    { memory: { role: 'harvester', spawnRoom: currentRoom.name } });
                }
                else if (harvesters.length < 2) {
                var newName = creepNames.harvesterNames[Math.floor(Math.random() * creepNames.harvesterNames.length)]+" Mini";
                //console.log('Spawning new harvester: ' + newName);
                availableSpawn.spawnCreep([WORK, MOVE], newName,
                    { memory: { role: 'harvester', spawnRoom: currentRoom.name } });
            }
        }
         /*If this all works correctly, it will spawn the smallest functional harvesters whenever I have fewer than 4 harvesters and less than 300 energy to spend.*/
        
        if (collectors.length < 3) {
            if (collectors.length < 1) {
                var newName = creepNames.collectorNames[Math.floor(Math.random() * creepNames.collectorNames.length)]+" Mini";
                //var newName = 'CollectorMini' + Game.time;
                availableSpawn.spawnCreep([CARRY, MOVE], newName,
                    { memory: { role: 'collector', spawnRoom: currentRoom.name } });
            }
            else if (currentRoom.energyAvailable > 500 && currentRoom.energyAvailable < 1000) {
                var newName = creepNames.collectorNames[Math.floor(Math.random() * creepNames.collectorNames.length)]+" Smol";
                //var newName = 'CollectorSmol' + Game.time;
                availableSpawn.spawnCreep([WORK, CARRY, CARRY, MOVE, MOVE, MOVE], newName,
                    { memory: { role: 'collector', spawnRoom: currentRoom.name } });
            }
            else if (currentRoom.energyAvailable > 1000) {
                var newName = creepNames.collectorNames[Math.floor(Math.random() * creepNames.collectorNames.length)]+" Md";
                availableSpawn.spawnCreep([WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], newName,
                    { memory: { role: 'collector', spawnRoom: currentRoom.name } });
            }
        }
        
        if(collectors.length > 0 && harvesters.length > 0) {
        if (builders.length < 2 && (currentRoom.energyAvailable < 899)) {
            var newName = creepNames.builderNames[Math.floor(Math.random() * creepNames.builderNames.length)]+" Md";
            //var newName = 'BuilderMd' + Game.time;
            //console.log('Spawning new builder: ' + newName);
            availableSpawn.spawnCreep([WORK, CARRY, CARRY, MOVE], newName,
                { memory: { role: 'builder', spawnRoom: currentRoom.name } });
        }
        
        if (builders.length < 2 && currentRoom.energyAvailable >= 1000) {
            var newName = creepNames.builderNames[Math.floor(Math.random() * creepNames.builderNames.length)]+" Big";
            //console.log('Spawning new builder: ' + newName);
            availableSpawn.spawnCreep([WORK, WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE], newName,
                { memory: { role: 'builder', spawnRoom: currentRoom.name } });
        }
        
        
        if (repairers.length < 1 && currentRoom.energyAvailable < 550 && builders.length > 0) {
            var newName = creepNames.repairerNames[Math.floor(Math.random() * creepNames.repairerNames.length)] +" Mini";
            //var newName = 'Repairer' + Game.time;
            //console.log('Spawning new repairer: ' + newName);
            availableSpawn.spawnCreep([WORK, CARRY, MOVE], newName,
                { memory: { role: 'repairer', spawnRoom: currentRoom.name } });
        }
        
        if (repairers.length < 1 && currentRoom.energyAvailable >= 550 && currentRoom.energyAvailable <= 1110) {
            var newName = creepNames.repairerNames[Math.floor(Math.random() * creepNames.repairerNames.length)];
            //var newName = 'Repairer' + Game.time;
            //console.log('Spawning new repairer: ' + newName);
            availableSpawn.spawnCreep([WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE], newName,
                { memory: { role: 'repairer', spawnRoom: currentRoom.name } });
        }
        
        if (repairers.length < 2 && currentRoom.energyAvailable >= 1111) {
            var newName = creepNames.repairerNames[Math.floor(Math.random() * creepNames.repairerNames.length)];
            //var newName = 'Repairer' + Game.time;
            //console.log('Spawning new repairer: ' + newName);
            availableSpawn.spawnCreep([WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE], newName,
                { memory: { role: 'repairer', spawnRoom: currentRoom.name } });
        }
        
        //console.log(currentRoom.energyAvailable);
        
        if (upgraders.length < 1 && builders.length > 0) {
            if (currentRoom.energyAvailable >= 1200) {
                var newName = creepNames.upgraderNames[Math.floor(Math.random() * creepNames.upgraderNames.length)];
                //console.log('Spawning new upgrader: ' + newName);
                availableSpawn.spawnCreep([WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE], newName,
                    { memory: { role: 'upgrader', spawnRoom: currentRoom.name } });
            }
            if (currentRoom.energyAvailable >= 300) {
                var newName = creepNames.upgraderNames[Math.floor(Math.random() * creepNames.upgraderNames.length)] +" Mini";
                //var newName = 'Upgradermini' + Game.time;
                //console.log('Spawning new upgrader: ' + newName);
                availableSpawn.spawnCreep([WORK, CARRY, MOVE, MOVE], newName,
                    { memory: { role: 'upgrader', spawnRoom: currentRoom.name } });
            }
        }
        
        
        if (defenders.length < 1 && currentRoom.energyAvailable >= 1000) {
            var newName = creepNames.defenderNames[Math.floor(Math.random() * creepNames.defenderNames.length)];
            //var newName = 'Defender' + Game.time;
            //console.log('Spawning new defender: ' + newName);
            availableSpawn.spawnCreep([TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK], newName,
                { memory: { role: 'defender', spawnRoom: currentRoom.name } });
        }
    }
        // if (scouts.length < 1 && currentRoom.energyAvailable >= 1750) {
        //     var newName = creepNames.scoutNames[Math.floor(Math.random() * creepNames.scoutNames.length)];
        //     //var newName = 'Scout' + Game.time;
        //     //console.log('Spawning new scout: ' + newName);
        //     availableSpawn.spawnCreep([MOVE, MOVE, CLAIM, CLAIM], newName,
        //         { memory: { role: 'scout', spawnRoom: currentRoom.name } });
        // }
        //Temporarily disabled scout spawning.

        // if (attackers.length < 3 && currentRoom.energyAvailable >= 2100) {
        //     var newName = creepNames.attackerNames[Math.floor(Math.random() * creepNames.attackerNames.length)];
        //     //var newName = 'Attacker' + Game.time;
        //     //console.log('Spawning new attacker: ' + newName);
        //     availableSpawn.spawnCreep([TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK], newName,
        //         { memory: { role: 'attacker', spawnRoom: currentRoom.name } });
        // }
        
        // if (attackers.length < 1 && currentRoom.energyAvailable >= 1200) {
        //     var newName = creepNames.attackerNames[Math.floor(Math.random() * creepNames.attackerNames.length)];
        //     //var newName = 'Attacker' + Game.time;
        //     //console.log('Spawning new attacker: ' + newName);
        //     availableSpawn.spawnCreep([TOUGH, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK], newName,
        //         { memory: { role: 'attacker', spawnRoom: currentRoom.name } });
        // }
        //Temporarily disabled attacker spawning

        // if (expanders.length < 1 && currentRoom.energyAvailable >= 1800) { //Hey add a condition here to check my number of rooms and spawns maybe?
        //     var newName = creepNames.expanderNames[Math.floor(Math.random() * creepNames.expanderNames.length)];
        //     //var newName = 'Expander' + Game.time;
        //     //console.log('Spawning new expander: ' + newName);
        //     availableSpawn.spawnCreep([MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, WORK, WORK, WORK], newName,
        //         { memory: { role: 'expander', spawnRoom: currentRoom.name } });
        // }
        //Temporarily disabled expander spawning

        if (availableSpawn.spawning) {
            var spawningCreep = Game.creeps[availableSpawn.spawning.name];
            availableSpawn.room.visual.text(
                '🤰' + spawningCreep.memory.role,
                availableSpawn.pos.x + 1,
                availableSpawn.pos.y,
                { align: 'left', opacity: 0.8 });
        }
    }



    





for (var name in Game.creeps) {
    var creep = Game.creeps[name];
    if (creep.memory.role == 'harvester') {
        roleHarvester.run(creep);
    }
    if (creep.memory.role == 'builder') {
        roleBuilder.run(creep);
    }
    if (creep.memory.role == 'upgrader') {
        roleUpgrader.run(creep);
    }
    if (creep.memory.role == 'repairer') {
        roleRepairer.run(creep)
    }
    if (creep.memory.role == 'defender') {
        roleDefender.run(creep)
    }
    if (creep.memory.role == 'collector') {
        roleCollector.run(creep)
    }
    if (creep.memory.role == 'scout') {
        roleScout.run(creep)
    }
    if (creep.memory.role == 'attacker') {
        roleAttacker.run(creep)
    }
    if (creep.memory.role == 'expander') {
        roleExpander.run(creep)
    }
}
//Below: intended to display some useful info periodically in the console.
//Need to update it to either display global totals for all rooms, or do that plus the numbers for each room.
// if (Game.time % 100 === 0) {
//     console.log(`Spendable energy: ${currentRoom.energyAvailable}
//      Attackers:  ${attackers.length}
//      Builders:   ${builders.length}
//      Collectors: ${collectors.length}
//      Defenders:  ${defenders.length}
//      Expanders:  ${expanders.length}
//      Harvesters: ${harvesters.length}
//      Repairers:  ${repairers.length}
//      Scouts:     ${scouts.length}
//      Upgraders:  ${upgraders.length}`);
// }
})}

