/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('mission.collect');
 * mod.thing == 'a thing'; // true
 */
//Used to take energy from containers/storage. If I want to later include extensions, probably best to make a separate mission.
module.exports = {

};