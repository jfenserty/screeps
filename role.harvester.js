/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */
//This guy now spawns, walks to a resource, and sits there dropping it on the ground for collectors to pick up.

var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
        // console.log(JSON.stringify(creep.pos));
        if(source == null)
            return;
        if(!creep.pos.inRangeTo(source.pos, 1)) {
            creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});
            return;
        }
        creep.harvest(source);
    }
};

module.exports = roleHarvester;