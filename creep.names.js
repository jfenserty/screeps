/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('creepNames');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    attackerNames:['Godzilla','Gojira','Mothra','Mosura','Gamera','Anguirus','Angilas','Baragon','Ghidorah','Rodan','MUTO','Stay-Puft Marshmallow Man','Spacegodzilla','Mechagodzilla','Manda','Gorosaurus','Gigan','Megalon','Titanosaurus','Kumonga','Minilla','Gabara','Biollante','Destoroyah'],
    //Theme: Kaiju
    builderNames:['Eugene V Debs','Martin Luther King Jr','Bernie Sanders','Karl Marx','Frederich Engels','Che Guevara','Helen Keller','Cesar Chavez','Alexandria Ocasio-Cortez'],
    //Theme: famous communists/socialists
    collectorNames:['Philip J Fry','Turanga Leela','Bender Bending Rodriguez','Hubert Farnsworth','Hermes Conrad','Amy Wong','Dr Zoidberg','Scruffy, the janitor','Mom','Cubert Farnsworth','Zapp Brannigan','Kif'],
    //Theme: Transporters/Drivers/Delivery People (Futurama)
    defenderNames:['The Iron Giant','Tengen Toppa Gurren Lagann','Jet Jaguar','Ultraman','Gipsy Danger','Megazord','Dragonzord','Voltron','Optimus Prime','Kiryu','MOGUERA', 'EVA-01'],
    //Theme: Mecha
    expanderNames:['Lex Luthor','Bruce Wayne','Tony Stark','John D. Rockefeller'],
    //Theme: idk
    harvesterNames:['Sleepy','Dopey','Doc','Grumpy','Sneezy','Bashful','Thorin','Crunchy','Stinky','Pinky','Yakko','Wakko','Dot'],
    //Theme: Dwarves (or miners)
    repairerNames:['Andy','Andrew','Andre','Andyana','Ando','Andie','Andylosaurus'],
    //Theme: Andy
    scoutNames:['Bingus','Maru','Gabe','Mango','Tofu','Miso','Laika','Checkers','Bobama','Major Biden','Hachiko','Chance','Sassy','Shadow','Shrek','Negro Matapacos','Snoopy','Odie','Garfield','garfielf','Fido'],
    //Famous pets
    upgraderNames:['Albert Einstein','Isaac Newton','George Washington Carver','Rosemary Grant','Rene Descartes','Galileo','Marie Curie','Stephen Hawking','Charlie Darwin','Copernicus','Richard Feynman','Gregor Mendel','Tycho Brahe','Bill Nye','Pascal','Alan Turing']
    //Theme: scientists
};


//scoutNames:['Scout','Jeremy','Jerma985','Tracer','Pit','Hermes','The Flash','Scout Cavalry','Hotshot','Quicksilver','Jeremy Elbertson','Sonic','The Quickster','QuikTrip','Captain Falcon','Fox','Deoxys-S','Ninjask','Jolteon','Electrode'],
    //Theme: fast guys